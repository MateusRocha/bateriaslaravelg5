    @extends('templates.base')

    @section('conteudo')
    <main>
        <h1>Turma 2D3 - Grupo 5</h1>
        <h2>Participantes</h2>
        <hr \>
        <table class="table table-striped table-bordered" id="tabela1">
            <tr>
                <th>Matrícula</th>
                <th>Nome</th>
                <th>Função</th>
            </tr>
            <tr>
                <td>0072557</td>
                <td>Leonardo Passos Sampaio</td>
                <td>Banco de dados, model, migrations e seeder</td>
            </tr>
            <tr>
                <td>0072530</td>
                <td>Mateus Rocha Oliveira</td>
                <td>Gerente, desenvolvimento css, rotas e controller</td>
            </tr>
            <tr>
                <td>0066066</td>
                <td>Victor Henrique Duarte Maia</td>
                <td>HTML e template blade</td>
            </tr>
        </table>
        <img class="grupo" src="imgs/fotogrupo.png" alt="Componentes do grupo">
    </main>

    @endsection

    @section('rodape')
        <h4>Rodapé da página principal</h4> 
        @endsection