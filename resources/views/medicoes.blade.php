@extends('templates.base')

@section('conteudo')
<main>
    <h1>Medições</h1>
    <hr>
    <h2>Resultados:</h2>
    <table class="table table-striped table-bordered" id="tbDados">
        <thead>
            <tr>
                <th>Pilha/Bateria</th>
                <th>Tensão nominal (V)</th>
                <th>Capacidade de corrente (mA.h)</th>
                <th>Tensão sem carga(V)</th>
                <th>Tensão com carga(V)</th>
                <th>Resitência de carga(ohm)</th>
                <th>Resistência interna(ohm)</th>
            </tr>
        </thead>
        <tbody>
            @foreach($medicoes as $medicoes)
            <tr>
                <td>{{$medicoes->pilha_bateria}}</td>
                <td>{{number_format($medicoes->tensao_nominal,1,'.','')}}</td>
                <td>{{$medicoes->capacidade_corrente}}</td>
                <td>{{$medicoes->tensao_sem_carga}}</td>
                <td>{{$medicoes->tensao_com_carga}}</td>
                <td>{{$medicoes->resistencia_carga}}</td>
                <td>{{number_format($medicoes->resistencia_interna,3,'.','')}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</main>
@endsection

@section('rodape')
<h4>Rodapé Medições</h4>
@endsection